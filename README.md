# grid-density-utility-metric

<h1>Utility metric for location data</h1>
<p>This algorithm enables to create a custom grid to analyze the distribution of geographical routes in an area.</p>

Install this package via pip:
```bash
pip install git+https://git@git.informatik.uni-leipzig.de/scads/de4l/privacy/grid-density-utility-metric.git
```