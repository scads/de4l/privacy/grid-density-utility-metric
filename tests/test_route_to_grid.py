"""Test of grid data structure.
"""

import unittest

from de4l_geodata.geodata.route import Route
from de4l_geodata.geodata.point import Point
import torch

from grid_density_utility_metric.route_to_grid import RouteToGrid, unify_grid


class TestGrid(unittest.TestCase):
    """Test of grid data structure.
    """

    def setUp(self) -> None:
        self.grid_cartesian = RouteToGrid(origin=Point([0, 0], geo_reference_system='cartesian'), grid_width=10,
                                          nr_grid_cells_per_row=10)
        self.route_coordinates = [[0.5, 0.5], [0.5, 0.5], [1.5, 0.5], [2.5, 2.5]]

    def test_init(self):
        """Test creating a grid with different geo reference systems.
        """
        self.assertRaises(ValueError, lambda: RouteToGrid(origin=[0, 0], grid_width=10, nr_grid_cells_per_row=10))

        for grid in [
            RouteToGrid(origin=Point([0, 0], geo_reference_system='cartesian'), grid_width=10,
                        nr_grid_cells_per_row=10),
            RouteToGrid(origin=Point([8, 41], geo_reference_system='latlon', coordinates_unit='degrees'), grid_width=1,
                        nr_grid_cells_per_row=50)]:
            try:
                grid
            except Exception:
                self.fail('Unexpected exception when creating a grid with specific geo reference system.')

    def test_zero_grid(self):
        """Test creating a grid with default value zero for each cell.
        """
        grid_zero = self.grid_cartesian.zero_grid()
        self.assertEqual(grid_zero.shape, torch.zeros([10, 10]).shape)
        self.assertEqual(torch.sum(grid_zero), 0)

    def test_route_to_grid_idx(self):
        """Test calculation of grid indices of a route.
        """
        route_coordinates = [[0.5, 0.5], [1.5, 0.5], [2.5, 2.5]]
        point_list = [Point(point, geo_reference_system='cartesian') for point in route_coordinates]
        route = Route(point_list)
        grid = RouteToGrid(origin=Point([0, 0]), grid_width=3, nr_grid_cells_per_row=3)
        expected_route_coordinates_in_grid = [[2, 0], [2, 1], [0, 2]]
        self.assertEqual(grid.route_to_grid_idx(route), expected_route_coordinates_in_grid)

    def test_route_to_grid(self):
        """Test that grid cells are filled correctly.
        """
        route = Route([[-8.585676, 41.148522], [-8.585712000000001, 41.148638999999996],
                       [-8.585685000000002, 41.148855000000005], [-8.585730000000002, 41.14892699999999]],
                      coordinates_unit='degrees')
        grid = RouteToGrid(origin=Point([-8.586159, 41.1484], coordinates_unit='degrees'),
                           grid_width=abs(41.1485 - 41.149176), nr_grid_cells_per_row=10)
        grid_cnt = grid.route_to_grid_count(route)
        self.assertEqual(4, torch.sum(grid_cnt))
        for (x_coordinate, y_coordinate) in [
            [2, 6],
            [3, 7],
            [6, 6],
            [8, 7]
        ]:
            self.assertEqual(1, grid_cnt[x_coordinate, y_coordinate])

    def test_route_to_grid_count(self):
        """Test that the route points are counted correctly per grid cell.
        """
        point_list = [Point(point, geo_reference_system='cartesian') for point in self.route_coordinates]
        route = Route(point_list)
        grid = RouteToGrid(origin=Point([0, 0]), grid_width=3, nr_grid_cells_per_row=3)
        grid_cnt = grid.route_to_grid_count(route)
        for (x_coordinate, y_coordinate, count) in [
            [2, 0, 2],
            [2, 1, 1],
            [0, 2, 1]
        ]:
            self.assertEqual(count, grid_cnt[x_coordinate, y_coordinate])

    def test_routes_to_grid_count(self):
        """Test that the route points of all routes are counted correctly per grid cell.
        """
        point_list = [Point(point, geo_reference_system='cartesian') for point in self.route_coordinates]
        route = Route(point_list)
        grid = RouteToGrid(origin=Point([0, 0]), grid_width=3, nr_grid_cells_per_row=3)
        grid_cnt = grid.routes_to_grid_count([route, route])
        for (x_coordinate, y_coordinate, count) in [
            [2, 0, 4],
            [2, 1, 2],
            [0, 2, 2]
        ]:
            self.assertEqual(count, grid_cnt[x_coordinate, y_coordinate])

    def test_unify_grid(self):
        """Test that non-zero grid values are transformed to one.
        """
        point_list = [Point(point, geo_reference_system='cartesian') for point in self.route_coordinates]
        route = Route(point_list)
        grid = RouteToGrid(origin=Point([0, 0]), grid_width=3, nr_grid_cells_per_row=3)
        grid_cnt = grid.routes_to_grid_count([route])
        unified_cnt = unify_grid(grid_cnt)
        self.assertEqual(3, torch.sum(unified_cnt))
        for (x_coordinate, y_coordinate, count) in [
            [2, 0, 1],
            [2, 1, 1],
            [0, 2, 1]
        ]:
            self.assertEqual(count, unified_cnt[x_coordinate, y_coordinate])
