"""Methods to span a grid over geographical data and analyze it.
"""

import torch
import numpy as np
from de4l_geodata.geodata.point import Point


def unify_grid(grid):
    """
    Transform non-zero grid values to one.

    Parameters
    ----------
    grid: torch.Tensor
        The tensor with same dimensions as this grid which is to be unified.

    Returns
    -------
    unified_grid: torch.Tensor
        A grid with all non-zero valued cells transformed to value one.
    """
    unified_grid = (grid > 0).int()
    return unified_grid


class RouteToGrid:
    """Create a grid and analyze geographical data with it.
    """
    def __init__(self, origin, grid_width, nr_grid_cells_per_row):
        """
        Construct a square grid layout, based on the indicated origin, width and resolution. The grid cells' values
        are given as a tensor with the grid's dimensions.

        Parameters
        ----------
        origin: Point
            The origin of the grid (being the lower left grid point).
        grid_width: float
            The extension of the grid. The value refers to the same geo reference system as the origin's, e.g. 'latlon'.
        nr_grid_cells_per_row: int
            The number of cells that the grid should have per row (and column). This variable defines the resolution
            of the grid.
        """
        super()
        if not isinstance(origin, Point):
            raise ValueError("Origin needs to be in Point format.")
        self.origin = origin
        self.grid_width = grid_width
        self.nr_grid_cells_per_row = nr_grid_cells_per_row

        self.cell_size = grid_width / nr_grid_cells_per_row
        self.upper_right = Point([self.origin.x_lon + grid_width, self.origin.y_lat + grid_width],
                                 geo_reference_system=self.origin.get_geo_reference_system(),
                                 coordinates_unit=self.origin.get_coordinates_unit())
        self.geo_reference_system = origin.get_geo_reference_system()

    def __str__(self):
        class_vars = [(key, val) for key, val in vars(self).items() if key in
                      ['origin', 'upper_right', 'grid_width', 'nr_grid_cells_per_row']]
        return str(class_vars)

    def zero_grid(self):
        """
        Return a grid where each cell has been initialized with zero.

        Returns
        -------
        torch.Tensor
            A tensor of zero values of the same dimensions as the grid.
        """
        return torch.zeros([self.nr_grid_cells_per_row, self.nr_grid_cells_per_row])

    def route_to_grid_idx(self, route):
        """
        Transform route coordinates into grid indices. The grid index coordinate system originates in the top left
        corner of the grid and axes point to the bottom (y-axis) and right (x-axis). Coordinates are given as [y, x].

        Parameters
        ----------
        route: de4l_geodata.geodata.route.Route
            The route to transform.

        Returns
        -------
        cell_indices: :obj:`list` of :obj:`tuple`
            A list of [y, x] coordinates that indicate which cells of this grid were touched by the routes' points.
        """
        cell_indices = []
        for point in route:
            # add route points only when they lie in grid boundaries
            if self.origin.y_lat <= point.y_lat < self.upper_right.y_lat \
                    and self.origin.x_lon <= point.x_lon < self.upper_right.x_lon:
                x_idx = self.nr_grid_cells_per_row - int(np.floor((point.y_lat - self.origin.y_lat) / self.cell_size)) \
                        - 1
                y_idx = int(np.floor((point.x_lon - self.origin.x_lon) / self.cell_size))
                cell_indices.append([x_idx, y_idx])
        return cell_indices

    def route_to_grid(self, route):
        """
        Map route points to this grid. Cells that are touched by a route point have value one.

        Parameters
        ----------
        route: de4l_geodata.geodata.route.Route
            The route to transform.

        Returns
        -------
        grid: torch.Tensor
            A grid with cells having value one when they are touched by a route point.
        """
        grid = self.route_to_grid_count(route)
        grid = unify_grid(grid)
        return grid

    def route_to_grid_count(self, route):
        """
        Count the route points per grid cell.

        Parameters
        ----------
        route: de4l_geodata.geodata.route.Route
            The route with points to be counted.

        Returns
        -------
        grid_cnt: torch.Tensor
            A grid containing the counts of route points per cell.
        """
        grid_cnt = self.zero_grid()
        cell_indices = self.route_to_grid_idx(route)
        for [x_coordinate, y_coordinate] in cell_indices:
            grid_cnt[x_coordinate, y_coordinate] += 1
        return grid_cnt

    def routes_to_grid_count(self, routes):
        """
        Count the route points of all routes per grid cell.

        Parameters
        ----------
        routes: :obj:`list` of :obj:`de4l_geodata.geodata.route.Route`
            A list of Route objects to be counted.

        Returns
        -------
        grid_cnt: torch.Tensor
            A grid containing the counts of all routes' points per cell.
        """
        grid_cnt = self.zero_grid()
        for route in routes:
            grid_cnt += self.route_to_grid_count(route)
        return grid_cnt

    def density_grid(self, routes, quantile):
        """
        Calculate the given quantile of point densities per grid cell.

        Parameters
        ----------
        routes: :obj:`list` of :obj:`de4l_geodata.geodata.route.Route`
            A list of Route objects to be taken into account for density calculation.
        quantile: float
            The quantile of densities.

        Returns
        -------
        density_grid: torch.Tensor
        A grid with point densities per grid cell, filtered by values lying inside the given quantile.
        """
        grid_cnt = self.routes_to_grid_count(routes)
        torch_quantile = torch.quantile(grid_cnt, torch.tensor([quantile]))
        density_grid = unify_grid(grid_cnt > torch_quantile)
        return density_grid
